package com.eightam.longrunningworkers

import android.app.Application
import android.util.Log
import androidx.core.app.NotificationManagerCompat
import androidx.work.Configuration
import androidx.work.WorkManager
import com.eightam.longrunningworkers.utils.notification.NotificationChannels
import com.eightam.longrunningworkers.utils.timer.TimerFactory
import com.eightam.longrunningworkers.utils.worker.AppWorkerFactory

class TimerApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        init()
    }

    private fun init() {
        initWorkManager()
        initNotificationChannels()
        initFactory()
    }

    private fun initWorkManager() {
        WorkManager.initialize(
            this,
            Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setWorkerFactory(AppWorkerFactory())
                .build()
        )
    }

    private fun initNotificationChannels() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManagerCompat.from(this).createNotificationChannel(
                NotificationChannels.getDefaultNotificationChannel(this)
            )
        }
    }

    private fun initFactory() {
        TimerFactory.init(this)
    }

}