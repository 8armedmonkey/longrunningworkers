package com.eightam.longrunningworkers.presentation.main

import androidx.lifecycle.*
import com.eightam.longrunningworkers.presentation.TimerPresentation
import com.eightam.longrunningworkers.presentation.toTimerPresentations
import com.eightam.longrunningworkers.timer.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class MainViewModel(
    private val getTimersLiveDataUseCase: GetTimersLiveDataUseCase,
    private val addTimerUseCase: AddTimerUseCase,
    private val removeTimerUseCase: RemoveTimerUseCase,
    private val removeAllTimersUseCase: RemoveAllTimersUseCase,
    private val notifyRunningTimersUseCase: NotifyRunningTimersUseCase
) : ViewModel() {

    fun start() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                notifyRunningTimersUseCase.execute()
            }
        }
    }

    fun addTimer(startAt: Date) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                addTimerUseCase.execute(startAt)
            }
        }
    }

    fun removeTimer(id: Long) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                removeTimerUseCase.execute(id)
            }
        }
    }

    fun removeAllTimers() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                removeAllTimersUseCase.execute()
            }
        }
    }

    fun getTimersLiveData(): LiveData<List<TimerPresentation>> =
        Transformations.map(getTimersLiveDataUseCase.execute()) { timers ->
            timers.toTimerPresentations()
        }

    class Factory(
        private val getTimersLiveDataUseCase: GetTimersLiveDataUseCase,
        private val addTimerUseCase: AddTimerUseCase,
        private val removeTimerUseCase: RemoveTimerUseCase,
        private val removeAllTimersUseCase: RemoveAllTimersUseCase,
        private val notifyRunningTimersUseCase: NotifyRunningTimersUseCase
    ) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return modelClass.cast(
                MainViewModel(
                    getTimersLiveDataUseCase,
                    addTimerUseCase,
                    removeTimerUseCase,
                    removeAllTimersUseCase,
                    notifyRunningTimersUseCase
                )
            ) ?: throw RuntimeException("Unable to create ViewModel: $modelClass")
        }

    }

}