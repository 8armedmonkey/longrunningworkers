package com.eightam.longrunningworkers.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.eightam.longrunningworkers.R

class TimerAdapter(
    private val onRemoveButtonClickedListener: (id: Long) -> Unit
) : ListAdapter<TimerPresentation, TimerAdapter.ViewHolder>(ItemCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_timer, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), onRemoveButtonClickedListener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val timerIdTextView = itemView.findViewById<TextView>(R.id.timer_id_text_view)
        private val timerStartAtTextView = itemView.findViewById<TextView>(R.id.timer_start_at_text_view)
        private val removeButton = itemView.findViewById<Button>(R.id.remove_timer_button)

        fun bind(
            timerPresentation: TimerPresentation,
            onRemoveButtonClickedListener: (id: Long) -> Unit
        ) {
            timerIdTextView.text = itemView.context.getString(
                R.string.timer_notification_title,
                timerPresentation.id.toString()
            )

            timerStartAtTextView.text = timerPresentation.getFormattedStartAt()

            removeButton.setOnClickListener {
                onRemoveButtonClickedListener.invoke(timerPresentation.id)
            }
        }

    }

    object ItemCallback : DiffUtil.ItemCallback<TimerPresentation>() {

        override fun areItemsTheSame(
            oldItem: TimerPresentation,
            newItem: TimerPresentation
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: TimerPresentation,
            newItem: TimerPresentation
        ): Boolean {
            return oldItem == newItem
        }

    }

}