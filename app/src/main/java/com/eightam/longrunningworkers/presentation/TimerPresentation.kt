package com.eightam.longrunningworkers.presentation

import com.eightam.longrunningworkers.timer.Timer
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

data class TimerPresentation(
    private val timer: Timer
) {

    val id: Long
        get() = timer.id

    fun getFormattedStartAt(): String {
        val format = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())
        return format.format(timer.startAt)
    }

    fun getFormattedDuration(currentTimeMillis: Long): String {
        val durationMillis = timer.getDurationMillis(currentTimeMillis)
        return String.format(
            "%02d:%02d:%02d",
            TimeUnit.MILLISECONDS.toHours(durationMillis) % 60,
            TimeUnit.MILLISECONDS.toMinutes(durationMillis) % 60,
            TimeUnit.MILLISECONDS.toSeconds(durationMillis) % 60
        )
    }

}