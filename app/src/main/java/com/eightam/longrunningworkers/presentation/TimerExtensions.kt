package com.eightam.longrunningworkers.presentation

import com.eightam.longrunningworkers.timer.Timer

fun Timer.toTimerPresentation(): TimerPresentation {
    return TimerPresentation(this)
}

fun List<Timer>.toTimerPresentations(): List<TimerPresentation> {
    return map { it.toTimerPresentation() }
}