package com.eightam.longrunningworkers.presentation.main

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.eightam.longrunningworkers.R
import com.eightam.longrunningworkers.presentation.TimerAdapter
import com.eightam.longrunningworkers.utils.timer.TimerFactory
import java.util.*

class MainActivity : AppCompatActivity() {

    private val adapter: TimerAdapter by lazy {
        TimerAdapter {
            viewModel.removeTimer(it)
        }
    }

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(
            this,
            MainViewModel.Factory(
                TimerFactory.getInstance().createGetTimersLiveDataUseCase(),
                TimerFactory.getInstance().createAddTimerUseCase(),
                TimerFactory.getInstance().createRemoveTimerUseCase(),
                TimerFactory.getInstance().createRemoveAllTimersUseCase(),
                TimerFactory.getInstance().createNotifyRunningTimersUseCase()
            )
        ).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpViews()
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        startViewModel()
    }

    private fun setUpViews() {
        findViewById<RecyclerView>(R.id.timer_recycler_view).adapter = adapter

        findViewById<Button>(R.id.add_timer_button).setOnClickListener {
            viewModel.addTimer(Date())
        }

        findViewById<Button>(R.id.remove_all_timers_button).setOnClickListener {
            viewModel.removeAllTimers()
        }
    }

    private fun observeViewModel() {
        viewModel.getTimersLiveData().observe(this, { timerPresentations ->
            timerPresentations?.let { adapter.submitList(it) }
        })
    }

    private fun startViewModel() {
        viewModel.start()
    }

}