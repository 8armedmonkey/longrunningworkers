package com.eightam.longrunningworkers.timer

import java.util.*
import kotlin.math.max

data class Timer(
    val id: Long,
    val startAt: Date
) {

    fun getDurationMillis(currentTimeMillis: Long): Long {
        return max(0, currentTimeMillis - startAt.time)
    }

}