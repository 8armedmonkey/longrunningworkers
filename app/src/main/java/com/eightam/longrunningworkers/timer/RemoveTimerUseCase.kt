package com.eightam.longrunningworkers.timer

class RemoveTimerUseCase(
    private val timerRepository: TimerRepository,
    private val timerNotificationService: TimerNotificationService
) {

    fun execute(id: Long) {
        timerRepository.removeTimer(id)
        timerNotificationService.notifyRunningTimers(timerRepository.getTimers())
    }

}