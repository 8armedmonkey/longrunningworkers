package com.eightam.longrunningworkers.timer

class RemoveAllTimersUseCase(
    private val timerRepository: TimerRepository,
    private val timerNotificationService: TimerNotificationService
) {

    fun execute() {
        timerRepository.removeAllTimers()
        timerNotificationService.notifyRunningTimers(timerRepository.getTimers())
    }

}