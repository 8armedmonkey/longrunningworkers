package com.eightam.longrunningworkers.timer

import java.util.*

class AddTimerUseCase(
    private val timerRepository: TimerRepository,
    private val timerNotificationService: TimerNotificationService
) {

    fun execute(startAt: Date) {
        timerRepository.addTimer(startAt)
        timerNotificationService.notifyRunningTimers(timerRepository.getTimers())
    }

}