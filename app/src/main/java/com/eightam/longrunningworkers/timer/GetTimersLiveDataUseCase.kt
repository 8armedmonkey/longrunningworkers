package com.eightam.longrunningworkers.timer

import androidx.lifecycle.LiveData

class GetTimersLiveDataUseCase(private val timerRepository: TimerRepository) {

    fun execute(): LiveData<List<Timer>> {
        return timerRepository.getTimersLiveData()
    }

}