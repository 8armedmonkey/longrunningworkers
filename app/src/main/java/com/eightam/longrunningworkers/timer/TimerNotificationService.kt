package com.eightam.longrunningworkers.timer

interface TimerNotificationService {

    fun notifyRunningTimers(timers: List<Timer>)

}