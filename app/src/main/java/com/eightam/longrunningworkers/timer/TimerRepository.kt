package com.eightam.longrunningworkers.timer

import androidx.lifecycle.LiveData
import java.util.*

interface TimerRepository {

    fun getTimersLiveData(): LiveData<List<Timer>>

    fun getTimers(): List<Timer>

    fun getTimer(id: Long): Timer?

    fun addTimer(startAt: Date): Long

    fun removeTimer(id: Long)

    fun removeAllTimers()

}