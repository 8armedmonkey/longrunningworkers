package com.eightam.longrunningworkers.timer

class NotifyRunningTimersUseCase(
    private val timerRepository: TimerRepository,
    private val timerNotificationService: TimerNotificationService
) {

    fun execute() {
        timerNotificationService.notifyRunningTimers(timerRepository.getTimers())
    }

}