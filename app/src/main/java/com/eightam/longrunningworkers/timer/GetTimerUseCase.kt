package com.eightam.longrunningworkers.timer

class GetTimerUseCase(private val timerRepository: TimerRepository) {

    fun execute(id: Long): Timer? {
        return timerRepository.getTimer(id)
    }

}