package com.eightam.longrunningworkers.utils.timer

import android.app.Notification
import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.ForegroundInfo
import androidx.work.WorkerParameters
import com.eightam.longrunningworkers.R
import com.eightam.longrunningworkers.presentation.TimerPresentation
import com.eightam.longrunningworkers.presentation.toTimerPresentation
import com.eightam.longrunningworkers.timer.GetTimerUseCase
import com.eightam.longrunningworkers.utils.notification.NotificationChannels.DEFAULT_CHANNEL_ID
import kotlinx.coroutines.delay

class TimerWorker(
    private val getTimerUseCase: GetTimerUseCase,
    context: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(context, workerParams) {

    override suspend fun doWork(): Result {
        val timerId = inputData.getLong(KEY_TIMER_ID, 0L)
        val timer = getTimerUseCase.execute(timerId)

        if (timer != null) {
            val timerPresentation = timer.toTimerPresentation()
            setForeground(createForegroundInfo(timerPresentation))

            while (timer.getDurationMillis(System.currentTimeMillis()) > 0) {
                NotificationManagerCompat.from(applicationContext).notify(
                    getTimerNotificationId(timerPresentation),
                    createTimerNotification(timerPresentation)
                )
                delay(DELAY_MILLIS)
            }
        }
        return Result.success()
    }

    private fun getTimerNotificationId(timerPresentation: TimerPresentation): Int {
        return timerPresentation.id.toInt()
    }

    private fun createTimerNotification(timerPresentation: TimerPresentation): Notification {
        return NotificationCompat.Builder(applicationContext, DEFAULT_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_notification_timer)
            .setContentTitle("Timer-${timerPresentation.id}")
            .setContentText(timerPresentation.getFormattedDuration(System.currentTimeMillis()))
            .setGroup(NOTIFICATION_GROUP_ID)
            .setOngoing(true)
            .setAutoCancel(false)
            .setShowWhen(false)
            .setOnlyAlertOnce(true)
            .setNotificationSilent()
            .build()
    }

    private fun createForegroundInfo(timerPresentation: TimerPresentation): ForegroundInfo {
        return ForegroundInfo(
            getTimerNotificationId(timerPresentation),
            createTimerNotification(timerPresentation)
        )
    }

    companion object {

        private const val KEY_TIMER_ID = "timerId"
        private const val DELAY_MILLIS = 1000L
        private const val NOTIFICATION_GROUP_ID = "timer"

        fun createInputData(timerId: Long): Data {
            return Data.Builder().putLong(KEY_TIMER_ID, timerId).build()
        }

    }

}