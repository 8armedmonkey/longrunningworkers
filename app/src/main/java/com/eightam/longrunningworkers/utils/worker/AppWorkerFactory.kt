package com.eightam.longrunningworkers.utils.worker

import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import com.eightam.longrunningworkers.utils.timer.TimerFactory
import com.eightam.longrunningworkers.utils.timer.TimerWorker

class AppWorkerFactory : WorkerFactory() {

    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters
    ): ListenableWorker? {
        return when (workerClassName) {
            TimerWorker::class.java.name -> TimerWorker(
                TimerFactory.getInstance().createGetTimerUseCase(),
                appContext,
                workerParameters
            )
            else -> null
        }
    }

}