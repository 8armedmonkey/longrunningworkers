package com.eightam.longrunningworkers.utils.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query

@Dao
interface TimerDao {

    @Query("SELECT * FROM timer")
    fun getTimersLiveData(): LiveData<List<TimerEntity>>

    @Query("SELECT * FROM timer")
    fun getTimers(): List<TimerEntity>

    @Query("SELECT * FROM timer WHERE id = :id")
    fun getTimer(id: Long): TimerEntity?

    @Query("INSERT INTO timer (startAtMillis) VALUES (:startAtMillis)")
    fun insertTimer(startAtMillis: Long): Long

    @Query("DELETE FROM timer WHERE id = :id")
    fun deleteTimer(id: Long)

    @Query("DELETE FROM timer")
    fun deleteAllTimers()

}