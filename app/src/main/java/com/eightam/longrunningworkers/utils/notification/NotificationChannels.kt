package com.eightam.longrunningworkers.utils.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import com.eightam.longrunningworkers.R

object NotificationChannels {

    const val DEFAULT_CHANNEL_ID = "default"

    @RequiresApi(Build.VERSION_CODES.O)
    fun getDefaultNotificationChannel(context: Context): NotificationChannel {
        return NotificationChannel(
            DEFAULT_CHANNEL_ID,
            context.getString(R.string.notification_channel_name_default),
            NotificationManager.IMPORTANCE_DEFAULT
        ).apply {
            setShowBadge(false)
            enableVibration(false)
            enableLights(false)
        }
    }

}