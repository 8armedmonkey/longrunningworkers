package com.eightam.longrunningworkers.utils.timer

import android.content.Context
import androidx.core.text.isDigitsOnly
import androidx.work.*
import com.eightam.longrunningworkers.timer.Timer
import com.eightam.longrunningworkers.timer.TimerNotificationService
import java.util.*

class TimerNotificationServiceImpl(
    private val context: Context
) : TimerNotificationService {

    private val workManager: WorkManager
        get() = WorkManager.getInstance(context)

    override fun notifyRunningTimers(timers: List<Timer>) {
        val runningTimerIds = timers.map { it.id }
        val timerIdsWithRunningWorkers = mutableSetOf<Long>()
        val workerIdsToBeCancelled = mutableSetOf<UUID>()
        val workQuery = WorkQuery.Builder.fromTags(listOf(TAG_TIMER_WORKER))
            .addStates(listOf(WorkInfo.State.RUNNING))
            .build()

        workManager.getWorkInfos(workQuery).get().forEach { workInfo ->
            val timerId = try {
                workInfo.tags.firstOrNull { it.isDigitsOnly() }?.toLong()
            } catch (e: Exception) {
                null
            }

            if (timerId != null && runningTimerIds.contains(timerId)) {
                if (workInfo.state.isFinished.not()) {
                    timerIdsWithRunningWorkers.add(timerId)
                }
            } else {
                workerIdsToBeCancelled.add(workInfo.id)
            }
        }

        workerIdsToBeCancelled.forEach { workManager.cancelWorkById(it) }

        runningTimerIds.forEach { timerId ->
            if (timerIdsWithRunningWorkers.contains(timerId).not()) {
                workManager.enqueue(
                    OneTimeWorkRequest.Builder(TimerWorker::class.java)
                        .setConstraints(Constraints.NONE)
                        .addTag(TAG_TIMER_WORKER)
                        .addTag(timerId.toString())
                        .setInputData(TimerWorker.createInputData(timerId))
                        .build()
                )
            }
        }
    }

    companion object {

        private const val TAG_TIMER_WORKER = "TimerWorker"

    }

}