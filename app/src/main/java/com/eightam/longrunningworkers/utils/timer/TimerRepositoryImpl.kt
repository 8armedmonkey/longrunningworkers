package com.eightam.longrunningworkers.utils.timer

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.eightam.longrunningworkers.timer.Timer
import com.eightam.longrunningworkers.timer.TimerRepository
import com.eightam.longrunningworkers.utils.room.TimerDatabase
import com.eightam.longrunningworkers.utils.room.toTimer
import com.eightam.longrunningworkers.utils.room.toTimers
import java.util.*

class TimerRepositoryImpl(
    private val database: TimerDatabase
) : TimerRepository {

    override fun getTimersLiveData(): LiveData<List<Timer>> {
        return database.timerDao().getTimersLiveData().map { it.toTimers() }
    }

    override fun getTimers(): List<Timer> {
        return database.timerDao().getTimers().toTimers()
    }

    override fun getTimer(id: Long): Timer? {
        return database.timerDao().getTimer(id)?.toTimer()
    }

    override fun addTimer(startAt: Date): Long {
        return database.timerDao().insertTimer(startAt.time)
    }

    override fun removeTimer(id: Long) {
        database.timerDao().deleteTimer(id)
    }

    override fun removeAllTimers() {
        database.timerDao().deleteAllTimers()
    }
}