package com.eightam.longrunningworkers.utils.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [TimerEntity::class], version = 1)
abstract class TimerDatabase : RoomDatabase() {

    abstract fun timerDao(): TimerDao

    companion object {

        const val DATABASE_NAME = "timer_database"

    }

}