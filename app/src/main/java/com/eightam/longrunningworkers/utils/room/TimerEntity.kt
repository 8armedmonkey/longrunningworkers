package com.eightam.longrunningworkers.utils.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "timer")
data class TimerEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "startAtMillis") val startAtMillis: Long
)