package com.eightam.longrunningworkers.utils.room

import com.eightam.longrunningworkers.timer.Timer
import java.util.*

fun TimerEntity.toTimer(): Timer {
    return Timer(
        id = id,
        startAt = Date(startAtMillis)
    )
}

fun List<TimerEntity>.toTimers(): List<Timer> {
    return map { it.toTimer() }
}