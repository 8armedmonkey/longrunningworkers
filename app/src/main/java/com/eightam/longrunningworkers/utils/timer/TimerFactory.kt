package com.eightam.longrunningworkers.utils.timer

import android.content.Context
import androidx.room.Room
import com.eightam.longrunningworkers.timer.*
import com.eightam.longrunningworkers.utils.room.TimerDatabase

class TimerFactory private constructor(private val context: Context) {

    private val timerDatabase: TimerDatabase by lazy {
        Room.databaseBuilder(
            context,
            TimerDatabase::class.java,
            TimerDatabase.DATABASE_NAME
        ).build()
    }

    private val timerRepository: TimerRepository by lazy {
        TimerRepositoryImpl(timerDatabase)
    }

    private val timerNotificationService: TimerNotificationService by lazy {
        TimerNotificationServiceImpl(context)
    }

    fun createGetTimersLiveDataUseCase(): GetTimersLiveDataUseCase {
        return GetTimersLiveDataUseCase(timerRepository)
    }

    fun createGetTimerUseCase(): GetTimerUseCase {
        return GetTimerUseCase(timerRepository)
    }

    fun createAddTimerUseCase(): AddTimerUseCase {
        return AddTimerUseCase(timerRepository, timerNotificationService)
    }

    fun createRemoveTimerUseCase(): RemoveTimerUseCase {
        return RemoveTimerUseCase(timerRepository, timerNotificationService)
    }

    fun createRemoveAllTimersUseCase(): RemoveAllTimersUseCase {
        return RemoveAllTimersUseCase(timerRepository, timerNotificationService)
    }

    fun createNotifyRunningTimersUseCase(): NotifyRunningTimersUseCase {
        return NotifyRunningTimersUseCase(timerRepository, timerNotificationService)
    }

    companion object {

        private lateinit var instance: TimerFactory

        fun init(context: Context) {
            if (Companion::instance.isInitialized) {
                throw IllegalStateException("Instance has been initialized.")
            } else {
                instance = TimerFactory(context)
            }
        }

        fun getInstance(): TimerFactory {
            if (Companion::instance.isInitialized) {
                return instance
            } else {
                throw IllegalStateException("Instance has not been initialized.")
            }
        }

    }

}